<?php
/**
 * @file
 * picec_product_shipping.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function picec_product_shipping_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_product_dimensions'.
  $field_bases['field_product_dimensions'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_product_dimensions',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'physical',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'physical_dimensions',
  );

  // Exported field_base: 'field_product_weight'.
  $field_bases['field_product_weight'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_product_weight',
    'indexes' => array(
      'weight' => array(
        0 => 'weight',
      ),
    ),
    'locked' => 0,
    'module' => 'physical',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'physical_weight',
  );

  return $field_bases;
}
